---
# People widget.
widget: people  # See https://sourcethemes.com/academic/docs/page-builder/
headless: true  # This file represents a page section.
active: false  # Activate this widget? true/false
weight: 30  # Order that this section will appear.

title: "People"
subtitle: ""

content:
  # List user groups to display.
  #   Edit each user's `user_groups` to add them to one or more of these groups.
  user_groups:
    - Core Team
    - Founder
    - Employees
    - Students
    - Administration
    - Visitors
    - Alumni

design:
  # Show user's social networking links? (true/false)
  show_social: true
  # Show user's interests? (true/false)
  show_interests: true
  # Show user's role?
  show_role: true
  # Show user's organizations/affiliations?
  show_organizations: true
---
