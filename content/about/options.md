---
# An instance of the Blank widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 10

# Section title
title: About Us

# Section subtitle
subtitle:

# Section design
design:
  # Use a 1-column layout
  columns: "1"
  #background:
  #  gradient_start: '#dc3545'
  #  gradient_end: '#007bff'
---

Founded in 2013, BinaryMist Ltd (the company behind PurpleTeam-Labs) is a small focussed consultancy specialising in providing Software Engineering and Development Teams with information security knowledge, experience, guidance and tooling to positively shape their DevSecOps culture, processes, practises and build pipelines.

Our aim is to help you shift the expensive process of finding and fixing security defects late in the development cycle (ambulance at the bottom of the cliff) to the cheapest place (as code is being written).

BinaryMist is empowering Software Development shops to not only significantly reduce software defects as they are being created, but also reducing the costs of creating your desired outcomes.
