+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 60  # Order that this section will appear.

title = "Need More Information?"
subtitle = "<br>"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-home-additional-resources"
+++

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body pt-main">
        &nbsp;
        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title text-center">
                  What is DAST?
                </h4>
                <p class="card-text" style="padding-left: 0;">What is Dynamic Application Security Testing and how does it help us?</p>
                <div class="text-center">
                  <a href="https://speakerdeck.com/binarymist/security-regression-testing-on-owasp-zap-node-api?slide=4" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fab fa-speaker-deck" aria-hidden="true"></i>&nbsp;Talk Slides</a>
                  <a href="https://binarymist.io/event/owaspnzday-2019-talk-security-regression-testing-on-owasp-zap-node-api/" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-microphone-alt" aria-hidden="true"></i>&nbsp;Presentation</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title text-center">
                  What is PurpleTeam?
                </h4>
                <p class="card-text" style="padding-left: 0;">Learn about PurpleTeam's origin and our journey to where we are now.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div class="text-center">
                  <a href="https://binarymist.io/blog/2021/02/17/purpleteam-at-alpha/" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-blog" aria-hidden="true"></i>&nbsp;Blog Post</a>
                  <a href="https://binarymist.io/event/appsecnz-2021-talk-building-purpleteam-a-security-regression-testing-saas-from-poc-to-alpha/" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-microphone-alt" aria-hidden="true"></i>&nbsp;Presentation</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-4">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title text-center">
                  PurpleTeam Demo
                </h4>
                <p class="card-text" style="padding-left: 0;">Want to see PurpleTeam in action?<br>Contact us for a demo.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div class="text-center">
                  <a href="/contact/" class="btn btn-sm btn-outline-primary"><i class="fas fa-running" aria-hidden="true"></i>&nbsp;Get&nbsp;Started</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
