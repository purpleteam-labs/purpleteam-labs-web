---
# Display name
title: Kim Carter

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Founder of PurpleTeam-Labs

# Organizations/Affiliations to show in About widget
organizations:
- name: BinaryMist Limited
  url: https://binarymist.io
#- name: PurpleTeam-Labs
#  url: https://purpleteam-labs.com

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin

user_groups: ["Founder"]

# Short bio (displayed in user profile at end of posts)
bio: 20+ years experience providing Software Engineering Teams with the confidence needed to produce software with the right level of security.

# Interests to show in About widget
interests:
#- Artificial Intelligence
#- Computational Linguistics
#- Information Retrieval

# Education to show in About widget
#education:
#  courses:
#  #- course: PhD in Artificial Intelligence
#  #  institution: Stanford University
#  #  year: 2012
#  #- course: MEng in Artificial Intelligence
#  #  institution: Massachusetts Institute of Technology
#  #  year: 2009
#  #- course: BSc in Artificial Intelligence
#  #  institution: Massachusetts Institute of Technology
#  #  year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '/contact/'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/binarymist
- icon: github
  icon_pack: fab
  link: https://github.com/binarymist
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/carterkim/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

Kim is the published author of many [information security books](https://binarymist.io/publication/kims-selected-publications/) specifically targeting Software Engineers, DevOps Engineers and Architects.
He has hosted and been a guest on [many podcasts](https://binarymist.io/publication/#8) involving information security, including being a host for [Software Engineering Radio](http://www.se-radio.net/team/kim-carter/).

He has written over 100 [blog posts](https://binarymist.io/blog) covering software engineering, information technology and security topics. 
[Spoken](https://binarymist.io/event/) at many international conferences and run many workshops helping Software Engineers understand and improve their information security.

Kim Pioneered the [InfoSecNZ Slack](https://github.com/binarymist/InfoSecNZ). Served as the OWASP [New Zealand](https://owasp.org/www-chapter-new-zealand/) Chapter Lead for Christchurch for eight years, and helped pioneer the Christchurch Hacker Conference. Kim continues to consult with Software Engineering Teams on a daily basis to improve their security, quality and reduce their time to delivery.

"_It's my goal in creating PurpleTeam to bring what was the costly red teaming exercise into parallel with your development (blue team) cycles, not only making this exercise many times cheaper but also automating out much of the mundane security bug hunting and remediation effort_."


<!-- {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}. -->

