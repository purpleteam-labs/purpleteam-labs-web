---
widget: hero
headless: true  # This file represents a page section.
active: true  # Activate this widget? true/false
weight: 30  # Order that this section will appear.

# ... Put Your Section Options Here (title etc.) ...

# Hero image (optional). Enter filename of an image in the page folder.
hero_media: ''

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `cta`.
#   Remove a link/note by deleting a cta/note block.
cta:
  url: '/contact/'
  label: Get Started
  icon_pack: fas
  icon: running
cta_alt:
  url: '/doc/'
  label: View Documentation

# Note. An optional note to show underneath the links.
cta_note:
  label: ''
---

