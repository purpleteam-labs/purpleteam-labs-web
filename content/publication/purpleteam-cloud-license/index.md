---
title: "PurpleTeam Cloud License Terms of Use"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- last updated
#- Robert Ford

# Author notes (optional)
author_notes:
- "This PurpleTeam Cloud License Terms of Use was last updated on 20 December 2021"
#- "Equal contribution"

# When changes are made, copy contents to the licenses/ directory of any projects bound by this license.
date: "2021-12-20T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
#publication: In *Wowchemy Conference*
#publication_short: In *ICW*

abstract: 

# Summary. An optional shortened abstract.
summary: 

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
#- example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: example
---

These terms of use (“Terms”) apply between you as a natural person or a representative of a legal person and BinaryMist Limited, a New Zealand company registered as BINARYMIST LIMITED (4540038), as set forth in Your subscription or separate Order Form. These Terms apply to Your access and use of the services made available by BinaryMist Limited at its websites and or by other means (the “Services”). If you are accepting these Terms on behalf of another person, a company or other legal entity, you represent and warrant that you have full authority to bind that person, company or legal entity to these Terms. “Customer” and “You” refer to you as a natural person or the legal person that you represent.

Please read these Terms carefully before using the Service(s). If you do not agree to these Terms, you shall immediately stop using the PurpleTeam Service(s). BY SIGNING UP FOR AND USING THE SERVICE(S) YOU HEREBY AGREE TO BE BOUND BY THESE TERMS AND ANY ADDITIONAL TERMS INCORPORATED HEREIN BY REFERENCE (EXCEPT INSOFAR AS THESE TERMS ARE EXPLICITLY REFERENCED AND VARIED BY THE ORDER FORM), INCLUDING, BUT NOT LIMITED TO, THE ACCEPTABLE USE POLICY AND THE PRIVACY POLICY (COLLECTIVELY, THE “TERMS”).

<br>

# Definitions and Interpretation



“**Acceptable Use Policy**” means the [acceptable use policy](#acceptable-use-policy) referenced to in these Terms.

“**Affiliate**” means any entity that directly or indirectly controls, is controlled by, or is under common control with such party, where control means the possession, directly or indirectly, of the power to direct or cause the direction of the management or the policies of an entity.

“**Agreement**” means these Terms and your subscription or any applicable Order Forms executed hereunder, and any schedules or additional terms referenced to in these Terms.

“**Availability**” means the availability of the Service as defined in the Service Level Agreement, if applicable.

"**Confidential Information**” means all business or technical information whether it is received, accessed or viewed in writing, visually, electronically or orally,
including without limitation the Outcomes and Customer Data, technical information, including without limitation details of BinaryMist’s Service, marketing and business plans, databases, specifications,
formulations, tooling, prototypes, sketches, models, drawings, specifications, engineering information, samples, computer software (source and byte codes), including without limitation BinaryMist’s software, forecasts, identity of or details about actual or potential customers or projects, techniques, inventions, discoveries, know-how, personal data (within the meaning of applicable data protection laws) and trade secrets, provided that such information is identified as confidential or a reasonable person would know it is confidential from the circumstances of disclosure.

Confidential Information does not include information that:

a: Was known to the Receiving Party prior to the time of disclosure by the Disclosing Party;  
b: Was in the public domain (other than Licensed PurpleTeam source code) prior to the time of execution of this Agreement, or which comes into the public domain during the term of this Agreement through no fault or breach of the Agreement of the Receiving Party;  
c: Has been independently developed by the Receiving Party without reference to or use of the Confidential Information; or  
d: The Receiving Party is obliged to disclose by law, or by a governmental or administrative agency or body or decision by a court of law, but only then after the Receiving Party has notified the Disclosing Party of the required disclosure, if not such notification is prohibited by applicable law, court or government order. The Receiving Party will limit the disclosure of Confidential Information to the greatest extent possible under the circumstances.

“**Customer Data**” means data uploaded or otherwise provided by You or by a third party on Your behalf to the Service platform or BinaryMist, including the Outcomes.

"**Documentation**" means the user and technical documentation for the Service(s) provided by BinaryMist, and includes any update of that documentation.

“**Effective Date**” means the effective date of the Agreement, which is the start date of Your subscription to the Service as indicated in Your subscription account. However, if the Service is provided to You subject to an Order Form, the Effective Date is stated therein.

“**Intellectual Property Rights**” means all copyrights and related rights, design rights, registered designs, patents, trademarks and service marks (registered and unregistered), trade secrets, database rights, know-how, rights in confidential information and all other intellectual property rights throughout the world for the full term of the rights concerned, including any derivative works incorporating any of the foregoing that may be created or developed in connection with this Agreement.

“**Order Form**” means a binding call-off for Services executed under these Terms, specifying the scope, price and terms for provision of an individual Service, including, if applicable, any professional services.

“**Outcomes**” means the outcome generated by a completed Test, which are made available to You by the Service.

“**Party**” means either one of the parties to the Agreement, You or BinaryMist.

“**Privacy Policy**” means the PurpleTeam-Labs [privacy policy](/privacy/) referenced to in these Terms.

“**PurpleTeam**” shall mean the security regression testing platform provided by BinaryMist, including both PurpleTeam Local and PurpleTeam Cloud, as defined below.

“**PurpleTeam Cloud**” shall mean the Service as defined in these Definitions as well as any projects (infrastructure as code (IaC) or other) used to create or maintain PurpleTeam running in the cloud, the use of which is subject to this PurpleTeam Cloud License.

“**PurpleTeam Local**” shall mean the version of PurpleTeam components, available free of charge on [Github](https://github.com/purpleteam-labs), but not any projects (infrastructure as code (IaC) or other) used to create or maintain PurpleTeam Cloud.

“**Purpose**” means the purpose of the Service as described in Section 2.1.

"**Representative**” means in relation to each Party, and any of its Affiliate:

a: Its officers and employees that need to know the Confidential Information  
b: Its professional advisers or consultants who are engaged to advise that Party and/or any of its Affiliate  
c: Its contractors and sub-contractors engaged by that Party, and/or any of its Affiliate and  
d: Any other person to whom the other Party agrees in writing that Confidential Information may be disclosed, and which is in connection with or necessary for the fulfilment of the Agreement.

“**Service(s)**” means the software as a service (SaaS) based web security services made available by BinaryMist that tests and provides feedback on the security status of its customers’ systems or any related professional services. This includes all PurpleTeam-Labs software projects, components, source code, byte code.

“**Service Level Agreement**” means the Service Level Agreement applicable to customers if stated in the applicable Order Form.

“**Subscription Term**” means the duration of Your subscription of the Service as defined in Your account or as stated in the applicable Order Form.

“**System**” means an information technology asset of Customer, such as websites, APIs, applications, software and information technology environments.

“**Terms**” means these terms of use and all additional terms and schedules referenced to herein, such as the Privacy Policy, Acceptable use Policy, and, if applicable Service Level Agreement.

“**Test**” means the security and vulnerability scans of a given Customer System included in the Service. A Test may, depending on the type of service You are using, include, among other things, information gathering, crawling, fingerprinting, fuzz testing, deploying of test scripts and performing automated penetration testing.

“**Trial**” means a free of cost trial subscription period or proof of concept of the Service granted to You by BinaryMist.

"**Update**" means a new version of the Service released or made available to you by BinaryMist.

“**User**” means an individual user, who has been granted access to the Service by the Customer or its Affiliates in accordance with this Agreement.

<br>

# 1. License to PurpleTeam Local

The License for PurpleTeam Local is the Business Source License v.1.1 ("BSL License"). Please see the text of the PurpleTeam [BSL License](https://github.com/purpleteam-labs/purpleteam/blob/main/licenses/bsl.md) for full terms. PurpleTeam Local is a no-cost, entry-level license and as such, contains the following disclaimers: TO THE EXTENT PERMITTED BY APPLICABLE LAW, PURPLETEAM LOCAL IS PROVIDED ON AN “AS IS” BASIS. LICENSOR HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS OR IMPLIED, INCLUDING (WITHOUT LIMITATION) WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, AND TITLE. For clarity, the terms of this Agreement, other than the relevant Definitions in the previous section and this section 1 do not apply to PurpleTeam Local.

# 2. License to PurpleTeam Cloud

## 2.1. Purpose of Services

You acknowledge that the purpose of the Service is to reveal security vulnerabilities in Your Systems and that in furtherance of the purpose, BinaryMist may when performing a Test, among other things,
perform crawling, fuzz testing, deploy test scripts and perform automated penetration testing for the limited purpose of revealing security vulnerabilities in Your Systems (“Purpose”).
You agree and acknowledge that the provision of the Service, including performance of the Tests, in accordance with this Agreement, may lead to detrimental impact on Your systems and is made solely at Your risk, and that You are responsible for the initiation of all Tests and the outcome of the Tests and for any inconveniences, interruptions or other negative consequences thereof.

## 2.2. Your use of the Service

Subject to these Terms and Your subscription or separate Order Form, if applicable, and payment of all applicable fees,
BinaryMist grants You a non-exclusive, non-transferable, non-assignable and limited right to use the respective Services and Documentation during the subscription term for your own
internal business purposes only. You are authorized to permit use of the Service to:

a: Your own employees,  
b: Your Affiliates and their respective employees, and  
c: Any third-party consultants performing services as independent contractors or subcontractors on Your behalf and/or on behalf of Customer Affiliates, solely for the purpose of providing such services to You and/or Your Affiliates.

Certain areas of this Service are restricted from being accessed by you and BinaryMist may further restrict access by you to any areas of this Service, at any time, in absolute discretion.
Any user ID, token and password you may have for this Service are confidential and you must maintain confidentiality as well.

## 2.3. Acceptable Use of the Service

You shall, and shall procure that Your Affiliates shall:

a: Obtain all necessary authorizations, approvals and permissions for use of the Service in relation to the relevant System;  
b: Use the Service in full compliance with this Agreement;  
c: Be responsible for any acts or omissions by Users;  
d: Use the Service in accordance with all applicable laws and government regulations (including any local laws to which You are subject);  
e: Use the Service in compliance with the Acceptable Use Policy;  
f: Not make the Service available to any unauthorized third party, and promptly inform BinaryMist in the event of any suspected unauthorized access to or use of the Service;  
g: Not create or attempt to create any substitute service or service similar to the Service, by use of, reference to or access to, the Service or any of BinaryMist’s Intellectual Property Rights;  
h: Not sell, lend out, lease, transfer, assign, sub-license, distribute or permit access or use of the Services, or any part thereof, to any third party without BinaryMist’s prior written approval;  
i: Not interfere with, or disrupt the integrity or performance of the Service or any third party data contained therein;  
j: Not attempt to gain unauthorized access to the Service or its related systems or networks; and  
k: Not decompile, disassemble, or reverse-engineer the software included in the Service, subject to what follows from applicable law.

## 2.4. Suspension of Service

BinaryMist may suspend Your, Your Affiliates’ or an individual Users’ access to and use of the Service (in whole or in part) upon prior notice, if, in BinaryMist’s reasonable opinion, Your, Your Affiliate’s or any User’s use of the Service:

a: Poses a threat to the security, availability or integrity of the Service or any other customer environment,  
b: Is in violation of the explicit use rights, included in the Acceptable Use Policy, granted under these Terms or any Order Form or any applicable law governing the use of the Service, or:  
c: Poses a legal or third-party liability risk for BinaryMist.

BinaryMist shall limit the suspension disabling only such component, use or access to the Service that is unauthorized according to this Section 2.4. BinaryMist shall promptly reinstate the Service for the relevant Customer, Customer Affiliate or User, when the underlying cause is remedied.

## 2.5. Warranty

2.5.1 THE SERVICE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SERVICE IS WITH YOU. IF THE SERVICE PROVES DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

2.5.2 SUBJECT TO THE REPRESENTATIONS AND WARRANTIES PROVIDED IN SECTION 2.5.1, BINARYMIST EXPRESSLY DISCLAIMS ALL OTHER WARRANTIES AND REPRESENTATIONS TO THE FULLEST EXTENT POSSIBLE UNDER APPLICABLE LAWS,
WHETHER EXPRESS, IMPLIED, OR STATUTORY, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OF MERCHANTABILITY, TITLE OR FITNESS FOR A PARTICULAR PURPOSE.
BINARYMIST SPECIFICALLY MAKES NO WARRANTY THAT THE SERVICE SHALL BE FREE FROM DEFECTS OR INTERRUPTIONS OF USE OR THAT THE SERVICE WILL BE 100% SUCCESSFUL IN IDENTIFYING ALL POSSIBLE SECURITY THREATS OR WEAKNESSES.
WITHOUT LIMITING THE ABOVE, BINARYMIST DOES NOT WARRANT THAT THE SERVICES WILL MEET YOUR REQUIREMENTS OR THAT THE OPERATION OR RESULT OF THE SERVICES WILL BE FREE FROM INTERRUPTIONS OR ERRORS.
YOU ACKNOWLEDGE THAT BY USING THE SERVICE YOU SIMULATE A REAL SYSTEM INTRUSION AND YOU ARE AWARE OF AND TAKE FULL RESPONSIBILITY FOR ANY CONSEQUENCES THEREOF, INCLUDING ANY CONSEQUENCES ATTRIBUTABLE TO THE USE OF THE INFORMATION CONTAINED IN REPORTS GENERATED AS PART OF THE SERVICE.

## 2.6. Modifications to the Service

The content, functionality and features of the Service may change over time as BinaryMist continuously enhances and updates the Service. Subject to Your termination rights as set forth in Section 2.15.2,

a: While the Service remains in pre-release (alpha, beta) BinaryMist provides no assurance that the Service will not under go material changes  
b: It's expected that you as a customer will be on one of the release cycles specified in the documentation. Releases may or may not be deployed on the specified release dates without further notification

Without limiting the generality of the foregoing, BinaryMist reserves the right to delete or disable content or functionality of the Service in the event of any claims based on alleged infringements of any third-party Intellectual Property Rights.

## 2.7. Security

2.7.1 BinaryMist and the PurpleTeam Service is subject to security measures in line with best industry practice. BinaryMist will take reasonable steps and precautions against security breaches. BinaryMist will maintain appropriate technical and organizational measures to protect any data and information, including personal data and Confidential Information, that it collects, accesses, processes or receives from You within the scope of the Service against unauthorized or unlawful transfer, processing, alteration or access and against accidental loss, damage, processing, use, transfer or destruction.

2.7.2 BinaryMist shall notify You immediately, but no later than 72 hours, or such shorter time period as may be required under applicable law, after becoming aware of any security breach or potential security breach which affects Your Service or Your business or systems. Both BinaryMist and You shall use commercially reasonable efforts to cooperate with one another to address or remediate any such security breach or potential security breach.

## 2.8. Personal data processing

For the avoidance of any doubts, BinaryMist is the data controller for all personal data relating to Service User accounts (such as name, email and phone number). BinaryMist will process such personal data in accordance with applicable data privacy laws and PurpleTeam-labs Privacy Policy.

## 2.9. Service Level Agreement

The Service Level Agreement is applicable to customers if so stated in the separate Order Form applicable between You and BinaryMist.

## 2.10. Prices and Payment terms

2.10.1 In consideration of the provision of the Service and the licenses granted hereunder, You shall pay the fees for the Service, as specified on the PurpleTeam-Labs website or in a mutually agreed
Order Form. BinaryMist may change its fees at any time in its sole discretion provided that such changes will not take effect for You until the start of the next Subscription Term.

2.10.2 You must pay the Fees to BinaryMist.

2.10.3 BinaryMist will provide you with valid tax invoices monthly or annually in advance for the Fees due in the following month or year, as set out in the PurpleTeam-Labs pricing page or otherwise agreed in writing between you and BinaryMist.

2.10.4 The Fees exclude applicable Sales Tax, which you must pay on taxable supplies under this Agreement.

2.10.5 You must pay the Fees:

a: Upfront before your chosen period (monthly, yearly) of use  
b: Electronically in cleared funds without any set off or deduction except to the extent required by law. If you are required by law to make any deduction, you must pay BinaryMist any additional amount that is necessary to ensure receipt by BinaryMist of the full amount which BinaryMist would have received but for the deduction.

2.10.6 BinaryMist may charge interest on overdue amounts. Interest will be calculated from the due date to the date of payment (both inclusive) at a rate equal to the higher of 2% per month and the maximum amount permitted by law and in addition the costs of any recovery action taken by BinaryMist to recover the debt (including legal fees on a solicitor-client basis).

2.10.7 BinaryMist may increase the Fees by giving at least 30 days notice. If you do not wish to pay the increased Fees, you may terminate this Agreement on no less than 10 days notice, provided the notice is received by us before the effective date of the Fee increase. If you do not terminate this Agreement in accordance with this clause, you are deemed to have accepted the increased Fees.

## 2.11. Liability

2.11.1 To the maximum extent permitted by law:

a: You access and use the Service at your own risk; and  
b: BinaryMist is not liable or responsible to you or any other person for any claim, damage, loss, liability and cost under or in connection with this agreement, the Services, or your access and use of (or inability to access or use) the Service. this exclusion applies regardless of whether BinaryMist's liability or responsibility arises in contract, tort (including negligence), equity, breach of statutory duty, or otherwise.

2.11.2 To the maximum extent permitted by law and only to the extent clause 2.11.1 does not apply, the maximum aggregate liability of BinaryMist under or in connection with this agreement or relating to the Services, whether in contract, tort (including negligence), breach of statutory duty or otherwise, will be limited (at BinaryMist's option) to:

a: Remedying, repairing or replacing the service; and/or  
b: Refunding the fees paid by you in your most recent payment preceding the first event giving rise to liability.

2.11.3 Without limiting clause 2.11.1, BinaryMist is not liable to you under or in connection with this agreement for any:

a: Loss of profit, revenue, savings, business, data and/or goodwill; or  
b: Consequential, indirect, incidental or special damage or loss of any kind.

## 2.12. Intellectual Property

2.12.1 From the date of creation or development, BinaryMist owns all Intellectual Property Rights in:

a: The Services and the Documentation; and  
b: Any other item or material created, developed or provided by or on behalf of BinaryMist under or in connection with this Agreement.

2.12.2 If you provide BinaryMist with ideas, comments or suggestions relating to the Service or the Documentation (together feedback):

a: All Intellectual Property Rights in that feedback, and anything created as a result of that feedback (including new material enhancements, modifications or derivative works), are owned solely by BinaryMist; and  
b: BinaryMist may use or disclose any feedback for any purpose.

2.12.3 A Party receiving Confidential Information (the “Receiving Party”) from the other Party (the “Disclosing Party”) shall keep Confidential Information strictly confidential and not disclose such Confidential Information to any third party without the Disclosing Party’s prior written consent. The Receiving Party shall ensure that the Confidential Information is treated and stored carefully and appropriately so that the Confidential Information is not inadvertently made available to any third party or otherwise disclosed in breach of the Agreement.

2.12.4 The Receiving Party may only use the Confidential Information for the purpose of complying with the Agreement and undertakes not to use the Confidential Information for any other purpose whatsoever.

2.12.5 The Receiving Party may disclose the Confidential Information only to its Representatives that have a direct need to know it. The Receiving Party shall procure that such Representatives are bound by no less extensive obligations than those set out in these Terms. The Receiving Party shall be liable to the Disclosing Party pursuant to the provisions set forth in these Terms for any breach by its Representatives.

2.12.6 The Receiving Party may not copy, make transcriptions or recordings or in any other way reproduce or duplicate any document or other medium containing Confidential Information, without the Disclosing Party’s prior written consent.

## 2.13. Term

The term of the Agreement shall commence upon the date you signed up for the Service online or upon the Effective Date of the Order Form, and unless earlier terminated as provided herein, shall continue for the Subscription Term as set out in Your subscription or applicable Order Form. The Subscription Term will automatically renew for successive terms equal in duration to the initial Subscription Term unless You notify BinaryMist in writing before the expiration of the then-current Subscription Term that You do not wish to renew the Services for an additional Subscription Term.

## 2.14. Trial Period

2.14.1	BinaryMist may make the Service available to you for a free trial of up to 2 weeks from the Start Date (Trial Period).

2.14.2	Where you download, install, access and/or use the Service under a free trial, then:

a: This clause 2.14 applies; and  
b: These Terms apply except to the extent varied in this clause 2.14.

2.14.3 The Service is provided to you during the Trial Period on an as is basis, and, despite any other provision in these Terms, all conditions, warranties, guarantees and indemnities in relation to the Service are excluded by BinaryMist to the fullest extent permitted by law.

2.14.4	No Fees are payable for your access and use of the Service during the Trial Period. You must purchase access to the paid version of the Service if you wish to access and use the Service following expiry of the Trial Period.

2.14.5	Nothing in these Terms imposes any obligation:

a: On you, at the termination or expiry of the Trial Period, to purchase a license to the Service; or  
b: On BinaryMist:  
i: At the termination or expiry of the Trial Period, to provide you with a license to the Service; or  
ii: To maintain any feature or part of the trial version Service in any paid version of the Service.

## 2.15. Termination

2.15.1 Either Party may terminate the Agreement without further notice if the other Party materially breaches the terms of the Agreement and does not remedy such breach within thirty (30) calendar days of the date on which breaching Party receives written notice of such breach from the other Party. Additionally, either Party may terminate the Agreement without liability to the other Party if the other enters into compulsory or voluntary liquidation, ceases for any reason to carry on business, or takes or suffers any similar action that the other Party reasonably believes will materially impair its performance under the Agreement (including payment of fees).

2.15.2 You may terminate the Agreement with immediate effect upon written notice to BinaryMist, if BinaryMist changes the Service once out of pre-release (alpha, beta) according to Section 2.6 in a way which constitutes a material adverse change of the Service (in Your reasonable opinion). Your notice of termination shall be given within two (2) weeks of BinaryMist’s notice of the material adverse change.

2.15.3 BinaryMist may terminate the Agreement with immediate effect upon written notice to You, if a suspension event according to Section 2.4 has lasted for more than 30 days without being remedied by You.

2.15.4 Where a Party has the right to terminate the Agreement for cause, it may also terminate all outstanding Order Forms on the same termination ground, or alternatively (at its discretion) only terminate the Order Form to which the termination ground relates.

## 2.16. Effects of termination

2.16.1 Where You have terminated the Agreement for material breach by BinaryMist pursuant to Sections 2.15.1 or 2.15.2 (or under the Service Level Agreement, if applicable) You shall receive a pro rata refund of any prepaid and unused fees from BinaryMist.

2.16.2 Where BinaryMist has terminated the Agreement subject to Sections 2.15.1 or 2.15.3 above, any sum owed or due to BinaryMist shall be immediately payable and You shall not be entitled to any remuneration or compensation from BinaryMist.

2.16.3 Further, upon the termination of this Agreement for any reason:

a: Your rights hereunder shall terminate; and  
b: Each Party shall upon request return (or at the other Party’s option, destroy) any and all Confidential Information in that Party’s possession or control to the other Party within two weeks, with the exception of confidential information stored in back-ups or archives and which cannot without significant efforts be retrieved or that a Party is required to retain due to a legal or regulatory obligation.

## 2.17. Assignment

Neither Party may assign or otherwise transfer this Agreement without the other Party’s prior written consent, which will not be unreasonably withheld; provided, however, that either Party may transfer this Agreement to an Affiliate or to a third party in connection with a merger, sale of all (or substantially all) of its shares or other ownership or a corporate reorganisation upon prior written notice.

## 2.18. Force majeure

Neither Party shall be liable for failure to fulfil any obligations under the Agreement, when this is due to any event beyond the reasonable control of a Party and which were not foreseen at the time of execution of the Agreement, and which could not have been prevented or its effects avoided by use of reasonable actions, such as, explosion, fire, storm, earthquake, flood, drought, riots, strikes, civil disobedience, sabotage, terrorist acts, civil war or revolutions, war or government action (“Force Majeure”). Each Party will use commercially reasonable efforts to undertake all necessary and reasonable actions within its control in order to limit the extent of the damages and consequences of Force Majeure. The Party affected by such Force Majeure shall immediately inform the other Party in writing of the beginning and the end of such occurrence. If an event of Force Majeure continues for a period of thirty (30) days or more, either Party may, upon written notice to the other Party, terminate this Agreement and/or the relevant Order Form without any further liability on the part of either Party, except to pay for Services already supplied.

## 2.19. Modification of the Terms

BinaryMist may revise these Terms, of which the current version will be available on the PurpleTeam-Labs website. The revised Terms become effective once made available on the website. BinaryMist will notify its customers of any major changes to the Terms, such as when Your rights and/or obligations will significantly change, in which case You will have the opportunity to object by contacting your BinaryMist customer contact. If you continue to use the Services after a revision of Terms has become effective, you agree to be bound by the revised Terms.

## 2.20. Notices

Any notice or other communication under the Agreement shall be in writing and shall be sent by letter or e-mail to the said contact person and shall be deemed effectively given upon receipt thereof.

Notices to You shall be sent to the contact person and e-mail address stated in the Order Form, if You have one, or in Your subscription account. Notices to BinaryMist shall be sent to:

BinaryMist

Att. Legal

info@purpleteam-labs.com

## 2.21. Governing law and dispute resolution

2.21.1 BinaryMist is not liable to you for any failure to perform its obligations under this Agreement to the extent caused by events beyond its reasonable control.  
2.21.2 Any illegality, unenforceability or invalidity of a provision of this Agreement does not affect the legality, enforceability or validity of the remaining provisions of this Agreement.  
2.21.3 Any variation to this Agreement must be in writing and signed by both parties.  
2.21.4 This Agreement sets out everything agreed by the parties relating to the Service(s), and Support Services and supersedes and cancels anything discussed, exchanged or agreed prior. The parties have not relied on any representation, warranty or agreement relating to the Service and the Support Services that is not expressly set out in this Agreement, and no such representation, warranty or agreement has any effect.  
2.21.5 This Agreement is governed by, and must be interpreted in accordance with, the laws of New Zealand. Each party submits to the non-exclusive jurisdiction of the Courts of New Zealand in relation to any dispute connected with this Agreement.

## 2.22. Support Services

2.22.1 BinaryMist will provide you with the Support Services set out on the PurpleTeam-Labs website for your agreed subscription account, provided you have:

a: Paid all Fees due;  
b: Maintained a proper operating environment for the use of the Service in accordance with any guidance from BinaryMist, including in the Documentation; and  
c: Complied with this Agreement and the Documentation.

2.22.2 Where you consider on reasonable grounds that there is a material failure of the Service to perform in accordance with the Documentation (Issue), BinaryMist will:  

a: Provide the level of support for your subscription account in the form of consultation, assistance and advice in relation to the Issue; and  
b: Use reasonable efforts to assist in the resolution of the Issue (taking into account the nature and severity of the Issue).

2.22.3 The provision of support by BinaryMist under section 2.22.2 is conditional on you:

a: First using reasonable efforts to resolve the Issue by referring to the Documentation; and  
b: Contacting BinaryMist during the business hours publicized on its website, via the support contact details published on its website.

<br>

{{% callout note %}}
This PurpleTeam Cloud License Terms of Use was last updated on 20 December 2021
{{% /callout %}}

<br>

# Acceptable Use Policy

**Scope and Purpose of this Acceptable Use Policy**:

This Acceptable Use Policy (AUP) describes prohibited use of the web services provided by BinaryMist.
This AUP governs the use of BinaryMist’s Service(s) and is incorporated by reference into the Terms of Use.
This AUP applies to BinaryMist’s customers obtaining Services from BinaryMist (“User”). The purpose of this AUP is to protect BinaryMist, its Customers and other internet users from illegal,
disruptive and other harmful activities. BinaryMist may modify this AUP at any time, publishing the then current version on this website. You agree to the latest version of this AUP by using the Services.

## Prohibited Use:

You may not use the Service for any illegal, harmful or infringing activity (“Prohibited Activity”), or in connection with content, products or services that are illegal,
harmful, infringing or offensive (“Prohibited Content”) as further described below.

## Prohibited Activity:

Prohibited Activity is considered any use of the Services for activity that is illegal, harmful or infringing. Prohibited Activities include, but are not limited to:

* Using the Service to violate the privacy or personal data rights of other individuals
* Using the Service to interfere with or harm the confidentiality, integrity or availability of any BinaryMist system or a third party system without authorization
* Abusing your Service account, including number of concurrent _Test Sessions_ or scan minutes used per day, indicated in your subscription account or applicable Order Form
* Using the Service to infringe the patents, copyright, trademark rights or other intellectual property right of a third party
* Using the Service for any other illegal activity

## Prohibited Content:

Prohibited Content is considered any use of the Services in connection with products, services, material or other content (“Content”) that is illegal, harmful, infringing, offensive or obscene.
Prohibited Content includes, but is not limited to:

* Using the Service in connection with Content that is illegal, like illegal drugs, illegal weapons, illegal gambling, human trafficking, child pornography or illegal pornography or prostitution
* Using the Service in connection with Content that infringes on third party rights, such as privacy or personal data protection rights or intellectual property rights
* Using the Service in connection with Content that in BinaryMist’s consideration is harmful, offensive or obscene, regardless of the jurisdiction in which You operate, such as hateful, violent, fraudulent, defamatory, discriminatory, racist or threatening Content

## Consequences of Your Prohibited Use:

BinaryMist disclaims any obligation to pre-approve or continuously monitor any activity or content.
The mere fact that You have been granted access to BinaryMist’s Services does not waive BinaryMist’s right to claim a breach by You of this AUP.
BinaryMist may choose to act in case it finds out and reasonably considers that Your activity or content is or may be harmful to BinaryMist, its customers or other internet users and causes or
may cause damage. BinaryMist reserves the right to suspend Your access to the Service in case Your use constitutes Prohibited Activity or Prohibited Use as well as terminate the Agreement and seek compensation for damages, should such arise, in accordance with the Terms of Use.

<br>

{{% callout note %}}
This Acceptable Use Policy was last updated on 15 May 2021
{{% /callout %}}


