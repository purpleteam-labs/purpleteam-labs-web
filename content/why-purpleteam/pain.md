+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "Dev Teams Struggling with Security?"
subtitle = "<br>"
draft = false

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

<!-- 1. Pain -->

<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-bug fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Too Many Security Defects and Too Costly to Fix</h3>    
    <p>Many organisations spend many thousands of dollars on security defect remediation of the software projects they create. Usually this effort is also performed late in the development life-cycle, often even after the code is considered done. This fact makes the remediation effort very costly and often too short. Because of this there are many bugs left in the software that get deployed to production.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-clock fa-3x" style="color: #9B6BCC; margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Traditional Red Teaming <a href="https://speakerdeck.com/binarymist/security-regression-testing-on-owasp-zap-node-api?slide=48" target="_blank">Too Late</a></h3>    
    <p>The <a href="https://speakerdeck.com/binarymist/security-regression-testing-on-owasp-zap-node-api?slide=50" target="_blank">bottom line</a> with traditional penetration testing / Red Team exercises is that:</p>
    <p>They're too late.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-funnel-dollar fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Cost Correlation to Time Detected</h3>    
    <p>This results in the practise of finding and fixing security defects being too expensive due to the time it takes a Developer to first find the defective code, refamiliarise themselves with the logic, understand how to apply each fix without introducing new defects. Then manually retest.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-dumpster-fire fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Traditional Red Teaming? <a href="https://speakerdeck.com/binarymist/security-regression-testing-on-owasp-zap-node-api?slide=58" target="_blank">86% Security Defects Left</a> Not Fixed</h3>
    <p>Because it's so expensive, very few security defects actually get fixed. Often you're lucky if they even make it into a risk register.</p>
  </div>
</div>
<br><br><br>
<div class="text-center"><h4>Is this good enough for the users of your software?</h4></div>
