+++
# Date this page was created.
date = "2021-09-06"

# Project title.
title = "OWASP PurpleTeam: Full system Test Run"

profile = false


# Project summary to display on homepage.
summary = "Full System _Test Run_: including CLI, back-end components, _Job_ file, _Outcomes_ archive, reports, results and documentation."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["video", "getting-started-video", "intermediate-video"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
#links = [
#  {name = "Talk", url = "https://binarymist.io/event/appsecnz-2021-talk-building-purpleteam-a-security-regression-testing-saas-from-poc-to-alpha/", icon = "microphone-alt", icon_pack = "fas"},
#  {name = "Blog Post", url = "https://binarymist.io/blog/2021/02/17/purpleteam-at-alpha/", icon = "blog", icon_pack = "fas"},
#  {name = "Slides", url = "https://speakerdeck.com/binarymist/building-purpleteam-a-security-regression-testing-saas-from-poc-to-alpha", icon = "speaker-deck", icon_pack = "fab"},
#  {name = "Conference Talk Video", url = "https://www.youtube.com/watch?v=0RFmweM7bwM", icon = "video", icon_pack = "fas"},
#  {name = "Interview", url = "https://binarymist.io/publication/dotnetrocks-interview-owasp-purpleteam/", icon = "podcast", icon_pack = "fas"}
#]

# Does the project detail page use math formatting?
math = false

[image]
caption = ""
focal_point = "Smart"
preview_only = true

+++

Kim takes you through a full system _Test Run_ of all components set-up in the `local` (OWASP) environment, and touches on many topics along the way, such as:

{{< youtube ACuaP-ZToKw >}}

</br>

* docker-compose-ui hosting the [Stage Two containers](https://github.com/purpleteam-labs/purpleteam-s2-containers)
* sam local: hosting the [lambda functions](https://github.com/purpleteam-labs/purpleteam-lambda)
* The _orchestrator_, _Testers_, and _Emissaries_
* The _PurpleTeam_ CLI: Including the UI components and navigating through the _Tester_ screens (if running in [`uI`](https://github.com/purpleteam-labs/purpleteam#ui) mode)
* Project [Definitions](https://purpleteam-labs.com/doc/definitions/)
* _Job_ file [examples](https://github.com/purpleteam-labs/purpleteam/tree/main/testResources/jobs) and [schema](https://purpleteam-labs.com/doc/job-file/)
* `alertThreshold`s
* CLI logs
* Outcomes archive
  * _Emissary_ Report files
    * Defects, how to reproduce, how to fix
  * _Tester_ Result files
* Documentation for [Log and Outcomes files](https://purpleteam-labs.com/doc/log-and-outcomes-files/)





