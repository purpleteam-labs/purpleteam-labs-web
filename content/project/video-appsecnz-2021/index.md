+++
# Date this page was created.
date = "2021-05-20"

# Project title.
title = "AppSec NZ - Building PurpleTeam: From PoC to Alpha"

profile = false


# Project summary to display on homepage.
summary = "The journey from PoC to Alpha. Includes a walk-through of the PurpleTeam composition."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["video", "getting-started-video"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [
  {name = "Talk", url = "https://binarymist.io/event/appsecnz-2021-talk-building-purpleteam-a-security-regression-testing-saas-from-poc-to-alpha/", icon = "microphone-alt", icon_pack = "fas"},
  {name = "Blog Post", url = "https://binarymist.io/blog/2021/02/17/purpleteam-at-alpha/", icon = "blog", icon_pack = "fas"},
  {name = "Slides", url = "https://speakerdeck.com/binarymist/building-purpleteam-a-security-regression-testing-saas-from-poc-to-alpha", icon = "speaker-deck", icon_pack = "fab"},
  {name = "Conference Talk Video", url = "https://www.youtube.com/watch?v=0RFmweM7bwM", icon = "video", icon_pack = "fas"},
  {name = "Interview", url = "https://binarymist.io/publication/dotnetrocks-interview-owasp-purpleteam/", icon = "podcast", icon_pack = "fas"}
]

# Does the project detail page use math formatting?
math = false

[image]
caption = ""
focal_point = "Smart"
preview_only = true

+++

{{< youtube 0RFmweM7bwM >}}

