---
# Title, summary, and page position.
linktitle: Cloud
summary: PurpleTeam cloud (BinaryMist) environment.
weight: 40
icon: cloud
icon_pack: fas

# Page metadata.
title: Cloud
date: "2021-05-08T00:00:00Z"
type: book  # Do not modify.
---

<br>Everything in this sub-section applies to running the _PurpleTeam_ CLI (_purpleteam_) configured for the `cloud` environment. (AKA **BinaryMist _PurpleTeam_**)<br><br>

## PurpleTeam `cloud` Architecture

The following diagram shows how the _PurpleTeam_ components communicate. In the `cloud` environment all you need to be concerned with is the _PurpleTeam_ CLI and of course making sure your [System Under Test (_SUT_)](../definitions) is listening, in a clean state, and reachable from the Cloud:

<!-- https://gohugo.io/content-management/shortcodes/#figure -->
{{< figure src="doc/purpleteam_cloud_2021-08-min.png" alt="cloud architecture" title="Cloud Architecture (click to enlarge)" >}}

## Your System Under Test (SUT)

Make sure the Web application or API (_SUT_) that you wish to target is up and responding to requests.

If your _SUT_ is not yet ready, an option may be to use what the PurpleTeam-Labs development team uses for their _SUTs_ ([purpleteam-iac-sut](https://github.com/purpleteam-labs/purpleteam-iac-sut)). PurpleTeam-Labs use purpleteam-iac-sut for both `local` and `cloud` testing.

## PurpleTeam (CLI)

Details on installing the _PurpleTeam_ CLI, configuring and running can be found [here](https://github.com/purpleteam-labs/purpleteam/blob/main/README.md#contents)

Once you have the CLI running and if you want or need to debug it, details can be found [here](../local/workflow#front-end).

## Release Strategies

Customers can choose one of the following release cycle cadences to be on:








<!--
* `dev` - Latest and greatest (bleeding edge)
   * When we make updates to the CLI, the customer needs to update the CLI before their next _Test Run_
   * Only source code is pushed to Github, new releases of the CLI are not guaranteed to be published to NPM
* `2w`, `4w`, `12w` - Stay on your version for a specified amount of time. This approach allows you to plan for your updates every two, four, or twelve weeks
   * CLI source is released (tagged) and published to NPM if changes have been made
   * CLI resources (config.cloud.json, _Job_ file if updated) is published to your customer specific file share that PurpleTeam-Labs provides you

New Zealand dates:

| Release Cadence | 14 Dec   | 28 Dec   | 11 jan   | 25 Jan   | 08 Feb   | 22 Feb   | 08 Mar   |
|-----------------|----------|----------|----------|----------|----------|----------|----------|
| Fortnightly     | &#x2714; | &#x2714; | &#x2714; | &#x2714; | &#x2714; | &#x2714; | &#x2714; |
| Monthly         |          | &#x2714; |          | &#x2714; |          | &#x2714; |          |
| Quarterly       |          |          |          | &#x2714; |          |          |          |
-->




* `dev` - Latest and greatest (bleeding edge)
   * When we make updates to the CLI, the customer needs to update the CLI before their next _Test Run_
   * Only source code is pushed to Github, new releases of the CLI are not guaranteed to be published to NPM
* `2w` - Stay on your version for at least two weeks. This approach allows you to plan for your updates. There may or may not be a release on the specified date, we will let you know if we are planning on creating a release near the end of the previous week
   * CLI source is released (tagged) and published to NPM if changes have been made
   * CLI resources (config.cloud.json, _Job_ file if updated) is published to your customer specific file share that PurpleTeam-Labs provides you

New Zealand dates:

| Release Cadence    | 25 Jan   | 08 Feb   | 22 Feb   | 08 Mar   | 22 Mar   | 05 Apr   | 19 Apr   |
|--------------------|----------|----------|----------|----------|----------|----------|----------|
| (`2w`) Fortnightly | &#x2714; | &#x2714; | &#x2714; | &#x2714; | &#x2714; | &#x2714; | &#x2714; |

<!-- https://www.calculator.net/date-calculator.html -->

