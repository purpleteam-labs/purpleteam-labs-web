---
title: Frequently Asked Questions
linktitle: FAQ
summary: Frequently Asked Questions.
toc: true
type: book
date: "2021-08-31T00:00:00+01:00"
draft: false

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 140
---

<br>

{{< toc hide_on="xl" >}}

## Q: What are concurrent _Test Sessions_?

**A**: _Test Sessions_ are explained in the [Definitions](/doc/definitions/).
To help with that documentation there are example [_Job_ files](https://github.com/purpleteam-labs/purpleteam/tree/main/testResources/jobs) which you can read in conjunction with the [_Job_ File documentation](/doc/job-file/#included).
An `appScanner` _Test Session_ has `relationships` to one-to-many routes. Most people only need one or two _Test Sessions_ defined in their _Job_ file.
Concurrent _Test Sessions_ are just when you specify more than one _Test Session_ in your _Job_ file. These _Test Sessions_ are run concurrently in isolated _Testers_.
You can also create multiple _Job_ files to spread _Test Sessions_ out, and run those _Job_ files sequentially.

## Q: What's the deal with scan minutes?

**A**: Scan minutes apply to `cloud` users (those with a PurpleTeam account) only.
If you are on the Enterprise (or custom) plan, you may have chosen:

* Unlimited scan minutes which means your PurpleTeam back-end environment is always on and available for you to use.
* A limited number of scan minutes per day, with this option you also have the added option of "Split schedules".
Split schedules allow you to have your daily quota of scan minutes split up into multiple blocks, this is usually a cheaper option than "Unlimited (always on)"

If you are on a plan with limited scan minutes there are a few things to be aware of:

* You decide when you want your allocated scan minutes to be scheduled
* There is a warm-up period. It takes approximately 10 minutes, sometimes a little more for the underlying back-end infrastructure (AWS ECS) to become stable.
this means that the stage one containers hosting the PurpleTeam micro-services may be stopped and started during this period. Issuing PurpleTeam CLI `status` commands will let you know if the PurpleTeam _orchestrator_ (The first point of meaningful contact behind the AWS ApiGateway) is ready to take orders.
Bear in mind though that during this warm-up period even though the _orchestrator_ reports as being ready to take orders the AWS ECS scheduler may still stop and start the crucial stage one containers.
So you can only rely on the _orchestrator_'s responses as being meaningful in regards to starting _Test Runs_ after this initial warm-up period.
If you have a scheduled block of scan minutes, PurpleTeam-Labs have scheduled these to start 20 minutes before your requested time. This "should" allow enough time for the AWS ECS scheduler to have your stage one containers in a stable state, if this is not the case please let us know.

## Q: When can I start a _Test Run_

**A**: This is different for `local` vs `cloud` environments, for:

* `local`: For your first _Test Run_: As soon as the stage one containers are up (usually less than 10 seconds unless images need to be fetched). For subsequent _Test Runs_: Once the previous _Test Run_ has completed and the _orchestrator_ has issued it's `orchestrator is ready to take orders.` message.
* `cloud`: For your first _Test Run_: As soon as AWS ECS considers the stage one Task containing the stage one containers to be stable, this is usually within ten minutes of the EC2 container instances being started from the beginning of your block of scheduled scan minutes.
For subsequent _Test Runs_: Once the previous _Test Run_ has completed and the _orchestrator_ has issued it's `orchestrator is ready to take orders.` message. For the `cloud` environment this period is longer than it is for `local` because AWS ECS has to destroy stage two Services and their respective Tasks.

