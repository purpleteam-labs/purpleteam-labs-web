---
title: Quick Start
linktitle: Quick Start
summary: The two main options for getting your System(s) under Test.
type: book
date: "2019-05-05T00:00:00+01:00"
toc: true
draft: false

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 20
---

There are two options with getting your System(s) under Test:

* _PurpleTeam_ `cloud`. All back-end services are set-up and ready to go
* _PurpleTeam_ `local`. You set-up everything yourself

Both `cloud` and `local` use the same code base.

## PurpleTeam `cloud` (BinaryMist PurpleTeam)

The quickest way to get up and running with having your Web application and/or API under test is to take the _PurpleTeam_ `cloud` path. At a high level, these steps look like the following:

1. Obtain a PurpleTeam-Labs `cloud` account. We set-up and manage everything in the cloud for you. If you decide to take the `local` path instead, this will be your responsibility
2. Get the [_PurpleTeam_ CLI](https://github.com/purpleteam-labs/purpleteam) (_purpleteam_) on your system and [configure it](https://github.com/purpleteam-labs/purpleteam#configure)
3. Create a [_Job_](/doc/jobfile/). There are some examples [here](https://github.com/purpleteam-labs/purpleteam/tree/main/testResources/jobs)
4. [Start testing](https://github.com/purpleteam-labs/purpleteam#run)

_PurpleTeam_ CLI can be run manually, driven from your CI, or other builds, to continuously inform you of security regressions in the Web applications and APIs that you are developing. This way you can easily find and fix your defects as they are being introduced.

Follow all directions under [`cloud`](../cloud/).

## PurpleTeam `local` (OWASP PurpleTeam)

If you choose to go the _PurpleTeam_ `local` path, a non-trivial set-up is required to get up and running.

Follow all directions under [`local`](../local/).

# Optimal Sequence of Steps

Once you have decided you're going with `local` or `cloud`...

* `local` - You have been through the Local Set-up, respective README's, Workflow, the _PurpleTeam_ CLI is ready to run and you have a basic _Job_ file without any routes specified
* `cloud` - You have been through the PurpleTeam-Labs induction and signed up for a PurpleTeam-Labs account, the _PurpleTeam_ CLI is ready to run and you have a basic _Job_ file without any routes specified

Additional efforts after that will be mainly focussed on tweaking and modifying your _Job_ files. The general steps will be the following but not necessarily in this order:

1. Perform a _Test Run_ using the CLI
2. Add authentication if your System Under Test (SUT) requires it
3. Specify some routes to test 
4. Put the CLI into your build pipeline
5. Add and extend to your existing _Job_ file, there are many additional knobs and levers you can apply and tweak

## 1. Perform a _Test Run_ using the CLI

With a very basic `BrowserApp` _Job_ file without any routes specified _PurpleTeam_ will scan from the root of your web application recursively.
If you're starting with scanning your API, you will be using the `Api` schema and will need to provide the definitions for your API.

## 2. Add authentication if your SUT requires it

Many web applications and APIs require the user to authenticate to access various routes and end-points, think of PurpleTeam as another user.
There are many different ways this can happen. The easiest approach if possible is to disable as much of the authentication as possible in order to test your application or APIs core functionality, of course there are many cases where this is not possible.

The PurpleTeam App _Tester_ provides a set of default strategies for authenticating to your web applications and APIs. These are discussed further in the _Job_ File documentation.
If your authentication scenario is not currently covered by our current set of strategies, you can add or modify code to cover your scenario, strategies are usually not too difficult to add, and can then be specified in your _Job_ file.

[Next Steps](../next-steps/) provides more detail around authenticated scanning.

## 3. Specify some routes to test

This only applies if your System Under Test (SUT) is a `BrowserApp`.

There are details around this in the `BrowserApp` _Job_ File documentation. If you are testing an API, then you have already provided your API definition in your `Api` _Job_ file.

## 4. Put the CLI into your build pipeline

There are some details in the [CLI README](https://github.com/purpleteam-labs/purpleteam#npm-install-locally), it's also worth discussing this on one of our Slack channels.

## 5. Add and extend to your existing _Job_ file

Work through the many possible options in the _Job_ (`BrowserApp` or `Api`) File documentation.

