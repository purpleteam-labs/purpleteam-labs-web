+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "How it Works"
subtitle = "<br>The CLI can be run manually which provides a dashboard, but it's sweet spot is being inserted into Development Team's build pipelines and run in headless ([`noUi`](https://github.com/purpleteam-labs/purpleteam/blob/main/README.md/#configure-ui)) mode."
draft = false

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-product-how-it-works"
+++

<!-- 6. Overcome Objections -->

<div class="container text-center">
  <br>
  <div class="row">
    <div class="card-deck">
      <div class="card">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Standard Pricing">-->
        {{< figure src="product/HowItWorks_What_576.svg" theme="light" alt="What to test" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">What</h3>
        </div>
        <div class="card-body">
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span>
              <div>Developers specify <a href="/doc/job-file/" target="_blank">what to test</a></div>
            </li>
          </ul>
        </div>
      </div>
      <div class="card">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Pro Pricing">-->
        {{< figure src="product/HowItWorks_How_576.svg" theme="light" alt="How to test" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">How</h3>
        </div>
        <div class="card-body">
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span>
              <div>PurpleTeam works out "how to test"</div>
            </li>
          </ul>
        </div>
      </div>
      <div class="card">
        <!--<img src="blank400x200.png" class="card-img-top" alt="Custom Pricing">-->
        {{< figure src="product/HowItWorks_NoTests_576.svg" theme="light" alt="No tests to be written" width="576" >}}
        <div class="card-body" style="flex-grow: 0;">
          <h3 class="card-title" style="margin-top: 0;">No Tests</h3>
        </div>
        <div class="card-body">
          <ul class="list-group list-group-flush">
            <li class="list-group-item text-left pt-what-you-get pt-line-item">
              <span><i class="fa fa-check" aria-hidden="true"></i></span>
              <div>There are no more tests to be written</div>
            </li>
          </ul.>
        </div>
      </div>
    </div>
  </div>
</div>
