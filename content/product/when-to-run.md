---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 30

title: You Decide When to Run It<br><br>
subtitle: <br>

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
- description: 
  icon: file-code
  icon_pack: fas
  name: On Commit
- description: 
  icon: baby-carriage
  icon_pack: fas
  name: On Push
- description: 
  icon: adjust
  icon_pack: fas
  name: Daily/Nightly
- description: 
  icon: clock
  icon_pack: far
  name: Any Time


# Uncomment to use emoji icons.
#- icon = ":smile:"
#  icon_pack = "emoji"
#  name = "Emojiness"
#  description = "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
#- icon = "your-custom-icon-name"
#  icon_pack = "custom"
#  name = "Surfing"
#  description = "90%"

advanced:
  # Custom CSS.
  css_style: ""

  # CSS class.
  css_class: "pt-featurette-col-3333"
---
