+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 60  # Order that this section will appear.

title = "Composition"
subtitle = "<br>PurpleTeam under the hood"
draft = false

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-product-composition"
+++

<!-- 7. Uniqueness -->

## Architectural Overview

<br>

The following depicts the architecture of the `local` (OWASP) environment. The `cloud` (BinaryMist) environment has all the same PurpleTeam core components, with added authentication and SAM CLI and docker-compose-ui swapped for cloud alternatives. More details are provided in [this blog post](https://binarymist.io/blog/2021/02/17/purpleteam-at-alpha/) and our [documentation](/doc/cloud/).

<br>

{{< figure src="doc/purpleteam_local_2021-08-min.png" alt="purpleteam local" width="100%" >}}

<br>

<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-plug fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>PurpleTeam Architecture is Pluggable</h3>    
    <p>If you find that you need a different type of <em>Tester</em>, talk to us, we may even ask you to implement it and plug it in.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-terminal fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Purpleteam CLI</h3>    
    <p>The purpleteam CLI is what initiates the security testing for you. Think of it as the interface into the PurpleTeam service. It has two modes, interactive and headless.<br>The interactive mode is used when you want to drive the tests and see the results in real-time as your System Under Test (SUT) is being security tested. You can page through the different testers to see how they are progressing along with real-time progress meters and security defect counts.<br>The headless mode is used when you are running the CLI from another process such as your build pipelines.<br>Both modes deliver an <em>Outcomes</em> archive to the location of your choosing once the PurpleTeam service has completed your <em>Test Run</em>.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-database fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Redis</h3>    
    <p>Redis pub/sub is used to transfer <em>Tester</em> messages (live update data) from the <em>Tester</em> micro-services to the <em>Orchestrator</em>.
The <a href="/doc/definitions/" target="_blank"><em>Build User</em></a> can configure the purpleteam CLI to receive these messages via Server Sent Events (<code>sse</code>) or Long Polling (<code>lp</code>). The <em>Orchestrator</em> also needs to be configured to use either <code>sse</code> or <code>lp</code>.<br>With Long Polling (<code>lp</code>) if the CLI goes off-line at some point during the <em>Test Run</em> and then comes back on-line, no messages will be lost due to the fact that the <em>Orchestrator</em> persists the messages it's subscribed to back to Redis lists, then pops them off the given list as a <code>lp</code> request comes in, and returns them to the CLI.<br><code>lp</code> is request->response, <code>sse</code> is one way. In saying that, <code>lp</code> can be quite efficient as we are able to batch messages into arrays to be returned.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-sitemap fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Orchestrator</h3>
    <p>The <em>Orchestrator</em> is responsible for:</p>
    <ul>
      <li>Organising and supervising the <em>Testers</em></li>
      <li>Sending real-time <em>Tester</em> messages to the CLI via either <code>sse</code> or <code>lp</code></li>
      <li>Packaging and sending the <em>Outcomes</em> (test reports, test results) back to the CLI as they become available</li>
      <li><a href="https://f1.holisticinfosecforwebdevelopers.com/chap06.html#web-applications-countermeasures-lack-of-input-validation-filtering-and-sanitisation" target="_blank">Validating, filtering and sanitising</a> the <em>Build User</em>'s input</li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-vials fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Testers</h3>
    <p>Each <em>Tester</em> is responsible for:</p>
    <ul>
      <li>Obtaining resources, cleaning up and releasing resources once the <em>Test Run</em> is finished</li>
      <li>Starting and Stopping <a href="https://github.com/purpleteam-labs/purpleteam-s2-containers" target="_blank">Stage Two Containers</a> (hosted on docker-compose-ui for the <code>local</code> env, or hosted on AWS ECS for the <code>cloud</code> env) dynamically (via <a href="https://github.com/purpleteam-labs/purpleteam-lambda" target="_blank">Lambda Functions</a> hosted via sam cli in <code>local</code> env, or hosted on AWS Lambda in <code>cloud</code> env) based on the number of <em>Test Sessions</em> provided by the <em>Build User</em> in the <a href="/doc/definitions/" target="_blank"><em>Job</em></a> file which is sent from the CLI to the <em>Orchestrator</em>, then disseminated to the <em>Testers</em></li>
      <li>The actual (app, server, tls, etc) test plan</li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-terminal fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Sam Cli</h3>
    <p>Sam Cli stays running and listening for the <em>Tester</em> requests to run the lambda functions which start and stop the Stage Two Containers.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fab fa-docker fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>docker-compose-ui</h3>
    <p>docker-compose-ui is required to be running in order to start/stop it's hosted containers (it has access to the hosts Docker socket).</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-lg-1 align-self-top">
    <i class="fas fa-horse fa-3x" style="margin-top: 1.7rem;" aria-hidden="true"></i>
  </div>
  <div class="col-md-12 col-lg-11">
    <h3>Emissaries</h3>
    <p>These are the work horses of PurpleTeam, they do most of the work.</p>
  </div>
</div>