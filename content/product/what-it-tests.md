+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://wowchemy.com/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "What it tests"
subtitle = "<br>PurpleTeam finds security defects in your running web applications and APIs by attacking their external interfaces and providing immediate and continuous notification of what and where your security defects are, how to fix them, how to reproduce each test, along with tips for spotting similar issues in the future."
draft = false

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  #gradient_start = "DarkGreen"
  #gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  #text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["100px", "0", "100px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = "pt-product-what-it-tests"
+++

<!-- 6. Overcome Objections -->

<br>
<div class="row">
  <div class="order-md-last col-sm-12 col-md-4 align-self-top">
    <h2>
      New or existing projects<br><br>
    </h2>
  </div>
  <div class="col-sm-12 col-md-8 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_TesterThresholds_304cw.svg" alt="Adjust attack strength" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">Tester Attack Strengths and Alert Thresholds</h5>
            <p class="card-text">You can adjust the attack strengths and alert thresholds of the Testers.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_PurpleTeamThresholds_304cw.svg" alt="purpleteam alert thresholds" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">PurpleTeam Attack Strengths and Alert Thresholds</h5>
            <p class="card-text">You can add PurpleTeam alert thresholds, great for existing projects with existing security defects that you don't want to count toward a failed Test Run.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="row">
  <div class="col-4 align-self-top">
    <h2>
      PurpleTeam tests:
    </h2>
  </div>  
  <div class="col-8 align-self-top">
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_AuthApps_210cc.svg" alt="Authenticated applications" width="105" class="pt-tests" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">Authenticated Applications</h5>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt pt-thumbnail">
          {{< figure src="product/PT_SingleScreen_210cc.svg" alt="Single page applications" width="105" class="pt-tests" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">Single Page Applications</h5>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_SSLDeploy_210cc.svg" alt="TLS/SSL Deployments" width="105" class="pt-tests" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">TLS/SSL Deployments</h5>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_ServerConfig_210cc.svg" alt="Server Configurations" width="105" class="pt-tests" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">Server Configurations</h5>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_ServerHTML_210cc.svg" alt="Server Side HTML" width="105" class="pt-tests" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">Server Side HTML</h5>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_GraphQL_API_210cc.svg" alt="GraphQL APIs" width="105" class="pt-tests" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">GraphQL APIs</h5>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-3 pt-thumbnail">
          {{< figure src="product/PT_RestAPIs_210cc.svg" alt="REST APIs" width="105" class="pt-tests" >}}
        </div>
        <div class="col-9">
          <div class="card-body">
            <h5 class="card-title">REST APIs</h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
