const internals = {
  contactForm: undefined,
  validateContactPost: undefined,
  contactQuestion: '2 + 7',
  correctContactAnswer: '9'
};

internals.validateRealContactUser = () => {
  const { validateContactPost, correctContactAnswer } = internals;
  if (validateContactPost.value === correctContactAnswer) return true;
  validateContactPost.value = '';
  return false;
};

// Todo: internals.validateStaticmanUser

internals.attachPricingCheckboxHandler = () => {
  const ptPricingCheckbox = document.querySelector('.pt-pricing-tiers input[type=checkbox]#pricing-monthly-vs-anual');
  
  ptPricingCheckbox && ptPricingCheckbox.addEventListener('change', (event) => {
    const anualBilling = event.target.checked;
    
    const anualIndicator = document.querySelector('.pt-pricing-tiers .anual-indicator');
    const monthlyIndicator = document.querySelector('.pt-pricing-tiers .monthly-indicator');
    
    const standardPriceAnual = document.querySelector('.pt-pricing-tiers .card ul.pt-price.pt-standard li.card-title .pt-anual');
    const standardPriceMonthly = document.querySelector('.pt-pricing-tiers .card ul.pt-price.pt-standard li.card-title .pt-monthly');
    
    const proPriceAnual = document.querySelector('.pt-pricing-tiers .card ul.pt-price.pt-pro li.card-title .pt-anual');
    const proPriceMonthly = document.querySelector('.pt-pricing-tiers .card ul.pt-price.pt-pro li.card-title .pt-monthly');

    anualIndicator.hidden = !anualBilling;
    monthlyIndicator.hidden = anualBilling;
    
    standardPriceAnual.hidden = !anualBilling;
    standardPriceMonthly.hidden = anualBilling;
    proPriceAnual.hidden = !anualBilling;
    proPriceMonthly.hidden = anualBilling;
  })
};

internals.contactFormHandler = () => {
  let { contactForm } = internals;
  const  { contactQuestion, validateRealContactUser } = internals;
  contactForm = document.querySelector('form.pt-contact');
  internals.validateContactPost = document.querySelector('input[name="validateRealContactUser"].pt-post-contact-field');
  if (internals.validateContactPost && contactForm) {
    internals.validateContactPost.placeholder = contactQuestion;
    contactForm.onsubmit = validateRealContactUser;
    contactForm.action = contactForm.querySelector('.form-action-value').innerHTML;
    contactForm.querySelector('.pt-post-contact-field.js-note.label').hidden = true;
  }
};

// Todo: internals.staticmanFormHandler

internals.attachHandlers = () => {
  const { attachPricingCheckboxHandler, contactFormHandler } = internals;
  attachPricingCheckboxHandler();
  contactFormHandler();
};


// https://developer.mozilla.org/en-US/docs/Web/API/Document/DOMContentLoaded_event#checking_whether_loading_is_already_complete
document.readyState === 'loading' ? document.addEventListener('DOMContentLoaded', internals.attachHandlers) : internals.attachHandlers();

